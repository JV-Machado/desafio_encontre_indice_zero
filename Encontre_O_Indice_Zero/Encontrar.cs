﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Encontre_O_Indice_Zero
{
    class Encontrar
    {
        public int Contador { get; set; }
        public List<string[]> Lista_Sequencias { get; set; }
        public string[] Sequencia { get; set; }
        public int[] Cont { get; set; }
        public int Maior { get; set; }
        public int Posicao { get; set; }

        public Encontrar()
        {
            Contador = 0;
            Lista_Sequencias = new List<string[]>();
            Cont = new int[25];
            Sequencia = new string[] { "0", "1", "0", "0", "1", "0", "1", "1", "1", "0", "0", "1" };
        } 

        public void Operacoes()
        {
            int t = 0;
            for(int i = 0; i < Sequencia.Length; i++)
            {
                if(Sequencia[i] == "0")
                {
                    Sequencia = new string[] { "0", "1", "0", "0", "1", "0", "1", "1", "1", "0", "0", "1" };
                    Sequencia[i] = "1";
                    for(int j = 0; j < Sequencia.Length; j++)
                    {
                        if(Sequencia[j] == "1")
                        {
                            Cont[t]++;
                        }
                        else if(Sequencia[j] == "0" && Cont[t] > 0)
                        {
                            t++;
                        }
                        if (Cont[t] > Maior)
                        {
                            Maior = Cont[t];
                            Posicao = i+1; 
                        }
                    }
                    
                    Lista_Sequencias.Add(Sequencia);
                }       

            }
         
            Console.WriteLine($"Posição {Posicao}");
        }
    }
}
